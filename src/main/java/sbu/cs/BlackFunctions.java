package sbu.cs;


public class BlackFunctions
{
    public static String gatValueBy(String value, int action)
    {
        String out = "";

        switch(action)
        {
            case 1 : out = reverse(value);
                break;

            case 2 : out = makePair(value);
                break;

            case 3 : out = repeat(value);
                break;

            case 4 : out = shiftToRight(value);
                break;

            case 5 : out = reverseChars(value);
                break;
        }

        return out;
    }


    private static String reverse(String value)
    {
        return new StringBuilder(value).reverse().toString();
    }


    /**
     * making each char double
     * @param value
     */
    private static String makePair(String value)
    {
        String tmp = "";

        for (char c : value.toCharArray())
        {
            tmp += c + "" + c;
        }

        return tmp;
    }


    private static String repeat(String value)
    {
        return (value + value);
    }


    private static String shiftToRight(String value)
    {
        return value.charAt(value.length() - 1) +
                (new StringBuilder(value).deleteCharAt(value.length() - 1).toString());
    }

    /**
     * replacing characters from end
     * @param value
     */
    private static String reverseChars(String value)
    {
        String tmp = "";

        for (char c : value.toCharArray())
        {
            tmp += (char)('z' - (c - 'a'));
        }

        return tmp;
    }
}

