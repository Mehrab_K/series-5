package sbu.cs;

public class MyArrays
{
    public static int minIndex(int [] arr)
    {
        int minIndex = 0;

        for (int j = 1; j < arr.length; j++)
        {
            if (arr[j] < arr[minIndex])
            {
                minIndex = j;
            }
        }

        return minIndex;
    }

    public static void swap(int [] arr, int len1, int len2)
    {
        int temp = arr[len1];
        arr[len1] = arr[len2];
        arr[len2] = temp;
    }

    private static void merge(int [] arr, int left, int middle, int right)
    {
        int leftSubArraySize = middle - left + 1;
        int rightSubArraySize = right - middle;

        int Left[] = new int[leftSubArraySize];
        int Right[] = new int[rightSubArraySize];

        /**
         * initializing :
         */

        for (int i = 0; i < leftSubArraySize; ++i)
        {
            Left[i] = arr[left + i];
        }

        for (int j = 0; j < rightSubArraySize; ++j)
        {
            Right[j] = arr[middle + 1 + j];
        }

        /**
         * temp variables :
         */
        int i = 0, j = 0;
        int k = left;

        while (i < leftSubArraySize && j < rightSubArraySize)
        {
            if (Left[i] <= Right[j])
            {
                arr[k] = Left[i];
                i++;
            }

            else
            {
                arr[k] = Right[j];
                j++;
            }

            k++;
        }

        /**
         * Copy remaining elements of Left[] :
         */
        while (i < leftSubArraySize)
        {
            arr[k] = Left[i];
            i++;
            k++;
        }

        /**
         * Copy remaining elements of Right[] :
         */
        while (j < rightSubArraySize)
        {
            arr[k] = Right[j];
            j++;
            k++;
        }
    }

    public static void mergeSort(int [] arr, int start, int end)
    {
        if (start < end)
        {
            int middle =start + (end - start)/2;

            mergeSort(arr, start, middle);
            mergeSort(arr, middle + 1, end);

            merge(arr, start, middle, end);
        }
    }

    public static int binarySearch(int arr[], int left, int right, int value)
    {
        if (right >= left)
        {
            int mid = left + (right - left) / 2;

            if (arr[mid] == value)
            {
                return mid;
            }

            if (arr[mid] > value)
            {
                return binarySearch(arr, left, mid - 1, value);
            }

            return binarySearch(arr, mid + 1, right, value);
        }

        return -1;
    }

}
