package sbu.cs;


public class WhiteFunctions
{
    public static String getValueBy(String value1, String value2, int action)
    {
        String out = "";

        switch(action)
        {
            case 1 : out = merge(value1, value2);
                break;

            case 2 : out = append(value1, value2);
                break;

            case 3 : out = reverseAppend(value1, value2);
                break;

            case 4 : out = checkForEven(value1, value2);
                break;

            case 5 : out = sumAndMod(value1, value2);
                break;
        }

        return out;
    }


    private static String reverse(String value)
    {
        return new StringBuilder(value).reverse().toString();
    }


    private static String merge(String value1, String value2)
    {
        String tmp = "";
        int index = Math.min(value1.length(), value2.length());

        for(int i = 0; i < index; i++)
        {
            tmp += (value1.charAt(i) + "" + value2.charAt(i));
        }

        return tmp + value1.substring(index) + value2.substring(index);
    }


    private static String append(String value1, String value2)
    {
        return value1 + reverse(value2);
    }


    private static String reverseAppend(String value1, String value2)
    {
        return merge(value1, reverse(value2));
    }


    private static String checkForEven(String value1, String value2)
    {
        if(value1.length() % 2 == 0)
        {
            return value1;
        }

        return value2;
    }


    private static String sumAndMod(String value1, String value2)
    {
        String tmp = "";
        int index = Math.min(value1.length(), value2.length());

        for(int i = 0; i < index; i++)
        {
            char c = (char) (((value1.charAt(i) - 97 + value2.charAt(i) - 97) % 26) + 97);
            tmp += c;
        }

        return tmp + value1.substring(index) + value2.substring(index);
    }
}

