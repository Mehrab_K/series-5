package sbu.cs.square;

import sbu.cs.BlackFunctions;

public class BlueSquare extends Square
{
    private String upInput, downOutPut;
    private String leftInput, rightOutPut;

    public BlueSquare(int action)
    {
        super(action);
    }

    public void setUpInput(String upInput)
    {
        this.upInput = upInput;

        this.downOutPut = BlackFunctions.gatValueBy(upInput, super.getAction());
    }

    public String getDownOutPut()
    {
        return downOutPut;
    }

    public String getRightOutPut()
    {
        return rightOutPut;
    }

    public void setLeftInput(String leftInput)
    {
        this.leftInput = leftInput;

        this.rightOutPut = BlackFunctions.gatValueBy(leftInput, super.getAction());
    }
}

