package sbu.cs.square;


import sbu.cs.BlackFunctions;

public class GreenSquare extends Square
{
    private String input, outPut;

    public GreenSquare(int action)
    {
        super(action);
    }

    public String getOutPut()
    {
        return outPut;
    }

    public void setInput(String input)
    {
        this.input = input;

        this.outPut = BlackFunctions.gatValueBy(input, super.getAction());
    }
}

