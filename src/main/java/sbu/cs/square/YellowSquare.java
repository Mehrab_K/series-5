package sbu.cs.square;

import sbu.cs.BlackFunctions;

public class YellowSquare extends Square
{
    private String input, outPut;

    public YellowSquare(int action)
    {
        super(action);
    }

    public String getOutPut()
    {
        return outPut;
    }

    public void setInput(String input)
    {
        this.input = input;

        this.outPut = BlackFunctions.gatValueBy(input, super.getAction());
    }
}
