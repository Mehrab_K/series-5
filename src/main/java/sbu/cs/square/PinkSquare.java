package sbu.cs.square;


import sbu.cs.WhiteFunctions;

public class PinkSquare extends Square
{
    private String leftInput = "", upInput = "", outPut;

    public PinkSquare(int action)
    {
        super(action);
    }

    public void setLeftInput(String leftInput)
    {
        this.leftInput = leftInput;
        check();
    }

    public void setUpInput(String upInput)
    {
        this.upInput = upInput;
        check();
    }

    private void check()
    {
        if(this.leftInput.length() != 0 && this.upInput.length() != 0)
        {
            this.outPut = WhiteFunctions.getValueBy(leftInput, upInput, super.getAction());
        }
    }

    public String getOutPut()
    {
        return outPut;
    }
}
