package sbu.cs;


import sbu.cs.square.*;

public class App
{
    private Square[][] squares;
    private int [][] arr;
    private int n;

    public String main(int n, int[][] arr, String input)
    {
        this.arr = arr;
        this.n = n;
        squares = new Square[n][n];

        initialize();

        // input square
        ((GreenSquare)squares[0][0]).setInput(input);

        String outPut = outPut();

        return outPut;
    }

    /**
     * putting numbers in squares by initializing objects
     */
    private void initialize()
    {
        firstRowInit();
        middleRowsInit();
        lastRowInit();
    }

    private void firstRowInit()
    {
        for(int i = 0; i < n - 1; i++)
        {
            squares[n - n][i] = new GreenSquare(arr[n - n][i]);
        }

        squares[n - n][n - 1] = new YellowSquare(arr[n - n][n - 1]);
    }

    private void middleRowsInit()
    {
        for(int i = 1; i < n - 1; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(j == 0)
                {
                    squares[i][j] = new GreenSquare(arr[i][j]);
                    continue;
                }

                else if(j == n -  1)
                {
                    squares[i][j] = new PinkSquare(arr[i][j]);
                    continue;
                }

                squares[i][j] = new BlueSquare(arr[i][j]);
            }
        }
    }

    private void lastRowInit()
    {
        squares[n - 1][0] = new YellowSquare(arr[n - 1][0]);

        for(int i = 1; i < n; i++)
        {
            squares[n - 1][i] = new PinkSquare(arr[n - 1][i]);
        }
    }

    private void firstRowIO()
    {
        for(int i = 0; i < n; i++)
        {
            if(i == 0)
            {
                String outPut = ((GreenSquare)squares[0][0]).getOutPut();

                ((GreenSquare)squares[0][i + 1]).setInput(outPut);

                ((GreenSquare)squares[1][i]).setInput(outPut);

                continue;
            }

            else if(i == n - 1)
            {
                String out = ((YellowSquare)squares[0][i]).getOutPut();

                ((PinkSquare)squares[1][i]).setUpInput(out);

                continue;
            }



            String out = ((GreenSquare)squares[0][i]).getOutPut();

            if(squares[0][i + 1] instanceof GreenSquare)
            {
                ((GreenSquare)squares[0][i + 1]).setInput(out);
            }

            else
            {
                ((YellowSquare)squares[0][i + 1]).setInput(out);
            }

            ((BlueSquare)squares[1][i]).setUpInput(out);

        }
    }

    private void middleRowIO()
    {
        for(int i = 1; i < n - 1; i++)
        {
            for(int j = 0; j < n; j++)
            {
                if(j == 0)
                {
                    String out = ((GreenSquare)squares[i][j]).getOutPut();

                    ((BlueSquare)squares[i][j + 1]).setLeftInput(out);

                    if(squares[i + 1][j] instanceof GreenSquare)
                    {
                        ((GreenSquare)squares[i + 1][j]).setInput(out);
                    }

                    else
                    {
                        ((YellowSquare)squares[i + 1][j]).setInput(out);
                    }
                }

                else if(j == n - 1)
                {
                    String out = ((PinkSquare)squares[i][j]).getOutPut();

                    ((PinkSquare)squares[i + 1][j]).setUpInput(out);
                }

                else
                {
                    String rightOut = ((BlueSquare)squares[i][j]).getRightOutPut();
                    String downOut = ((BlueSquare)squares[i][j]).getDownOutPut();

                    if(squares[i][j + 1] instanceof BlueSquare)
                    {
                        ((BlueSquare)squares[i][j + 1]).setLeftInput(rightOut);
                    }
                    else if(squares[i][j + 1] instanceof PinkSquare)
                    {
                        ((PinkSquare)squares[i][j + 1]).setLeftInput(rightOut);
                    }

                    if(squares[i + 1][j] instanceof BlueSquare)
                    {
                        ((BlueSquare)squares[i + 1][j]).setUpInput(downOut);
                    }
                    else if(squares[i + 1][j] instanceof PinkSquare)
                    {
                        ((PinkSquare)squares[i + 1][j]).setUpInput(downOut);
                    }

                }
            }
        }
    }

    private void lastRowIO()
    {
        for(int i = 0; i < n - 1; i++)
        {
            if(i == 0)
            {
                String out = ((YellowSquare)squares[n - 1][i]).getOutPut();

                ((PinkSquare)squares[n - 1][i + 1]).setLeftInput(out);

                continue;
            }

            String out = ((PinkSquare)squares[n - 1][i]).getOutPut();

            ((PinkSquare)squares[n - 1][i + 1]).setLeftInput(out);
        }
    }

    private String outPut()
    {
        firstRowIO();
        middleRowIO();
        lastRowIO();

        return ((PinkSquare)squares[n - 1][n - 1]).getOutPut();
    }
}
