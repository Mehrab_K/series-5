package sbu.cs;

import java.util.*;

public class SortArray
{
    /**
     * sort array arr with selection sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] selectionSort(int[] arr, int size)
    {
        for (int i = 0; i < size - 1; i++)
        {
            // Finding min :
            int minIndex = MyArrays.minIndex(Arrays.copyOfRange(arr, i, arr.length));

            // Swap :
            MyArrays.swap(arr, minIndex + i, i);
        }

        return arr;
    }

    /**
     * sort array arr with insertion sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] insertionSort(int[] arr, int size)
    {
        for (int i = 1; i < size; ++i)
        {
            int number = arr[i];

            // sub array from 0 -> i - 1
            int subArrayIndex = i - 1;

            while (subArrayIndex >= 0 && arr[subArrayIndex] > number)
            {
                arr[subArrayIndex + 1] = arr[subArrayIndex];
                subArrayIndex -= 1;
            }

            arr[subArrayIndex + 1] = number;
        }

        return arr;
    }

    /**
     * sort array arr with merge sort algorithm
     *
     * @param arr  array of integers
     * @param size size of arrays
     * @return sorted array
     */
    public int[] mergeSort(int[] arr, int size)
    {
        MyArrays.mergeSort(arr, 0, size - 1);
        return arr;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in iterative form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearch(int[] arr, int value)
    {
        int start = 0;
        int end = arr.length - 1;

        while (start <= end)
        {
            int mid = (start + end)/2;
            int midValue = arr[mid];

            if (midValue < value)
            {
                start = mid + 1;
            }

            else if (midValue > value)
            {
                end = mid - 1;
            }

            else
            {
                return mid;
            }
        }

        return -1;
    }

    /**
     * return position of given value in array arr which is sorted in ascending order.
     * use binary search algorithm and implement it in recursive form
     *
     * @param arr   sorted array
     * @param value value to be find
     * @return position of value in arr. -1 if not exists
     */
    public int binarySearchRecursive(int[] arr, int value)
    {
        return MyArrays.binarySearch(arr, 0, arr.length - 1, value);
    }
}
